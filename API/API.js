import axios from 'axios'

/**
 * Fonction permettant d'appeler l'API qui retourne les films correspondant à la recherche.
 * @param {*} pSearch La recherche.
 * @returns Une liste de films.
 */
export function searchFilms(pSearch) {
    const API_KEY = 'ae962ef5'
    const url = 'http://www.omdbapi.com/'
    return new Promise((resolve, reject) => {
        return axios.get(url, {params: {
            s: pSearch,
            apiKey: API_KEY
        }}).then(response => {
            resolve(response.data)
        }).catch(error => {
            reject(error)
        });
    });
}

/**
 * Fonction permettant de récupérer un film via son imdb ID.
 * @param {*} imdbId L'id.
 * @returns Un film détaillé.
 */
 export function getFilm(imdbId) {
    const API_KEY = 'ae962ef5'
    const url = 'http://www.omdbapi.com/'
    return new Promise((resolve, reject) => {
        return axios.get(url, {params: {
            i: imdbId,
            plot: 'full',
            apiKey: API_KEY
        }}).then(response => {
            resolve(response.data)
        }).catch(error => {
            reject(error)
        });
    });
}
