import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Home from '../Components/Home'

const FilmStackNavigator = createStackNavigator({
     // Page d'accueil permettant d'afficher les résultats de la recherche.
     Home: {
        screen: Home,
        navigationOptions: {
            title: 'ESN films - Recherche'
        }
    },
})

export default createAppContainer(FilmStackNavigator)
