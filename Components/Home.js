import React from 'react';
import { View, StyleSheet, TextInput, Button, Text} from 'react-native'
import { searchFilms } from './../API/API.js'

/**
 * Component principal. Permet de rechercher un film et d'afficher ses informations.
 */
class Home extends React.Component {
    constructor(props) {
        super(props)
        this.searchedFilm = ''
        this.state = { films: [] }
    }

    /**
     * Fonction utilisée pour mettre la valeur saisie dans la variable searchedFilm.
     * @param {*} pFilm Le film saisie dans le champs texte.
     */
    setFilm(pFilm) {
        this.searchedFilm = pFilm
    }

    /**
     * Fonction pour lancer une recherche sur le film. Appelle l'API.
     */
    launchSearch() {
        searchFilms(this.searchedFilm).then(res => {
            console.log(res.Search)
            this.setState({films: res.Search})
        }).catch(err => {
            console.log(err)
        })
    }

    render() {
        return (
            <View style={styles.content}>
                <View style={styles.search}>
                    <TextInput style={styles.textInput} placeholder="Saisissez ici le nom d'un film'" onChangeText={(text => this.setFilm(text))}></TextInput>
                    <Button style={styles.searchButton} color="red" title="Rechercher"></Button>
                </View>
                <View style={styles.result}>
                    <Text>A remplacer par la liste des films</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1
    },
    search: {
        flex: 1,
        justifyContent: 'center',
    },
    result: {
        flex: 6,
        justifyContent: 'center',
    },
    textInput: {
        marginTop: 2,
        marginBottom: 5,
        padding: 5,
        fontSize: 18,
        backgroundColor: 'white'
    },

})
export default Home



